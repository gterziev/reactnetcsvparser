import React, { useEffect, useState } from 'react';
import Chart from "chart.js/auto";
import { CategoryScale } from "chart.js";
import { Bar } from "react-chartjs-2";
import axios from "axios";
import { Alert, ProgressBar } from "react-bootstrap";


Chart.register(CategoryScale);

export default function BarChart({ title, url }) {

    const [data, setData] = useState(null);
    const [chartData, setChartData] = useState(null);
    const [error, setError] = useState(null);
    const [errorMessage, setErrorMessage] = useState('');
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {

        axios.get(url)
            .then((response) => {

                //console.log(response);
                setData(response.data);
            }, (error) => {
                console.log(error);
                setErrorMessage(error);
            })
           .finally(function () {
                setIsLoading(false);
            });

    }, []);

    useEffect(() => {

        function buildDataset( barData, colors) {
            let dataset = {
                label: title,
                data: barData,
                backgroundColor: colors,
                borderColor: colors,
                borderWidth: 0.5
            };

            return dataset;
        }

        function loadChartData(data) {

            console.log("loadChartData");
            console.log(data);

            let barLabels = [];
            let barData = [];
            let colors = [];

            data.forEach((chartItem) => {

                barLabels.push(chartItem.label);
                barData.push(chartItem.number);
                colors.push(chartItem.color);
            });

            let barDatasets = [];
            barDatasets.push(buildDataset(barData, colors));

            let barChartData = { labels: barLabels, datasets: barDatasets };
            console.log(barChartData);

            setChartData(barChartData);
        }

        if (data) {
            loadChartData(data);
        }

    }, [data]);

    useEffect(() => {
        if (error) {
            setErrorMessage(error.message);
        }
    }, [error]);

    return (

        <>
            {error ? (
                <Alert key="danger" variant="danger" >
                    {errorMessage}!
                </Alert>
            ) : null}

            {chartData ? (

                <Bar
                    data={chartData}
                    
                />
            ) :null}
            
        </>
    );
};