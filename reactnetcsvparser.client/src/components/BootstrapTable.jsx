import axios from "axios";
import React, { useEffect, useState } from 'react'
import {Alert, ProgressBar} from "react-bootstrap";

import 'bootstrap/dist/css/bootstrap.min.css';
import Table from 'react-bootstrap/Table';

export default function BootstrapTable({ url}) {

    const [data, setData] = useState([]);
    const [error, setError] = useState();
    const [errorMessage, setErrorMessage] = useState();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {

        axios.get(url)
            .then((response) => {
                console.log(response);
                setData(response.data);
                setIsLoading(false);

            }, (error) => {
                console.log(error);
                setErrorMessage(error);
            })
            .finally(function () {
                setIsLoading(false);
            });

    }, []);


    return (
        <>

            {error ? (
                <Alert key="danger" variant="danger" >
                    {errorMessage}!
                </Alert>
            ) : null}
            

            {data ? (

                <Table striped bordered hover>

                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Upload Time</th>
                    </tr>
                    {data.map((val, key) => {
                        return (
                            <tr key={key}>
                                <td>{val.id}</td>
                                <td>{val.name}</td>
                                <td>{new Date(val.uploadTime).toLocaleString() }</td>
                            </tr>
                        )
                    })}
                </Table>) : null}
        </>
    );
}
