import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Nav, Navbar } from "react-bootstrap";
import { Route, Routes } from 'react-router-dom';

import Home from './pages/Home.jsx';
import Grid from './pages/Grid.jsx';
import Chart from './pages/Chart.jsx';
import NoMatch404 from './pages/NoMatch404.jsx';

function App() {

    const serverUrl = "http://localhost:5119/attachmentData";

    return (
        <>

            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="#home">Csv Parser</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav variant="tabs" className="me-auto" defaultActiveKey="/" >
                        <Nav.Link href="/" eventKey="home" title="Home">Home</Nav.Link>
                        <Nav.Link href="/grid" eventKey="grid" title="Grid">Grid</Nav.Link>
                        <Nav.Link href="/chart" eventKey="chart" title="Chart">Chart</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <Container className="tab-content">
                <Routes>
                    <Route path="/" element={<Home title="Csv Selector" url={ serverUrl} />} />
                    <Route path="/grid" element={<Grid title="Attachment list" url={serverUrl +"/getAttachments"} />} />
                    <Route path="/chart" element={<Chart title="Bar chart" url={serverUrl + "/getChartData"} />} />
                    <Route path="*" element={<NoMatch404 />} />
                </Routes>
            </Container>
        </>
    );
}

export default App;
