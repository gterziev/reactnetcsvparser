import DataFileSelector from '../components/DataFileSelector.jsx';

export default function Home({title, url}) {

  return (

      <>
          <h4>{title}</h4>
          <DataFileSelector title={title} url={url} />
      </>
  );
}