import BarChart from "../components/BarChart.jsx";

export default function Chart({title, url}) {

  return (

      <>
          <h4>{ title }</h4>
          <BarChart title={title} url={url} />
      </>
  );
}