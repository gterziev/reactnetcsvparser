import BootstrapTable from "../components/BootstrapTable.jsx";

export default function Grid({title, url}) {

  return (

      <>
          <h4>{ title }</h4>
          <BootstrapTable url={url} />
      </>
  );
}