﻿using CsvHelper;
using System.Globalization;
using System.Linq;
using ReactNetCsvParser.Server.Models;
using CsvHelper.Configuration;

namespace ReactNetCsvParser.Server.Services
{

    public interface IParseService<T>
    {
        List<T> Parse(TextReader reader);
    }

    public record CsvData(string Color, int Number, string Label);

    public class CsvParseService : IParseService<ChartItem>
    {
        private CsvConfiguration _config;

        public CsvParseService()
        {
            _config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                TrimOptions = TrimOptions.Trim,
                HasHeaderRecord = true
            };
        }

        public List<ChartItem> Parse(TextReader reader)
        {
            using var csv = new CsvReader(reader, _config);

            var csvData = csv.GetRecords<CsvData>();

            var chartItemList = new List<ChartItem>();
            foreach( var csvItem in csvData)
            {
                chartItemList.Add(new ChartItem() { Color = csvItem.Color, Number = csvItem.Number, Label = csvItem.Label });
            }

            return chartItemList;
        }

    }
}