﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReactNetCsvParser.Server.Data;
using ReactNetCsvParser.Server.Models;
using ReactNetCsvParser.Server.Services;
using System.Data;

namespace ReactNetCsvParser.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AttachmentDataController : Controller
    {
        private readonly WebDbContext _context;
        private readonly IParseService<ChartItem> _parseService;

        public AttachmentDataController(WebDbContext context, IParseService<ChartItem> parseService)
        {
            _context = context;
            _parseService = parseService;
        }

        // GET: AttachmentData
        [HttpGet("GetAttachments")]
        public async Task<IActionResult> Attachments()
        {
            try
            {
                var attachments = await _context.AttachmentData.OrderByDescending(a => a.UploadTime).ToListAsync();
                return Ok(attachments);
            }catch(Exception ex)
            {
                return Problem(ex.Message);
            }
            
        }

        // GET: ChartData
        [HttpGet("GetChartData")]
        public async Task<IActionResult> Chart()
        {
            try
            {
                var chartData = await _context.ChartItem.ToListAsync();
                return Ok(chartData);
            }
            catch(Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost(Name = "UploadFile")]
        public async Task<IActionResult> FileUpload(IFormFile SingleFile)
        {
            try
            {

                if (SingleFile == null || SingleFile.Length == 0)
                {
                    ModelState.AddModelError("NoFileUploaded", "File not selected");
                    return ValidationProblem(ModelState);
                }

                var permittedExtensions = new[] { ".csv" };
                var extension = Path.GetExtension(SingleFile.FileName).ToLowerInvariant();

                if (string.IsNullOrEmpty(extension) || !permittedExtensions.Contains(extension))
                {
                    ModelState.AddModelError("UnsupportedFileType", $"Provided extension {extension} is not valid file type. Please choose csv file.");
                }

                //Validating the File Size
                int permittedFileSize = 1000000; // Limit to 1 MB
                if (SingleFile.Length > permittedFileSize)
                {
                    ModelState.AddModelError("FileTooLarge", $"File size exceeds permitted file size of {permittedFileSize} bytes.");
                }

                //is file already uploaded
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "uploads", SingleFile.FileName);
                if (Path.Exists(filePath))
                {
                    ModelState.AddModelError("FileAlreadyExists", $"File {SingleFile.FileName} is already uploaded.");
                }

                if (ModelState.IsValid)
                {

                    //Using Buffering
                    using (var stream = System.IO.File.Create(filePath))
                    {
                        // The file is saved in a buffer before being processed
                        await SingleFile.CopyToAsync(stream);
                    }

                    //Using Streaming
                    //using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                    //{
                    //    await SingleFile.CopyToAsync(stream);
                    //}

                    // Parse csv data 
                    var chartDataList = _parseService.Parse(new StreamReader(SingleFile.OpenReadStream()));
                    var attachmentData = new AttachmentData()
                    {
                        Name = SingleFile.FileName,
                        UploadTime = DateTime. UtcNow,
                        ChartDataList = chartDataList
                    };

                    _context.Add(attachmentData);
                    await _context.SaveChangesAsync();

                    return Ok("Upload successfull");
                }
                else
                {
                    return ValidationProblem(ModelState);
                }
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
            
        }

        // GET: AttachmentData/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var attachmentData = await _context.AttachmentData
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (attachmentData == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(attachmentData);
        //}

        // POST: AttachmentData/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var attachmentData = await _context.AttachmentData.FindAsync(id);
        //    if (attachmentData != null)
        //    {
        //        _context.AttachmentData.Remove(attachmentData);
        //    }

        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool AttachmentDataExists(int id)
        //{
        //    return _context.AttachmentData.Any(e => e.Id == id);
        //}

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
