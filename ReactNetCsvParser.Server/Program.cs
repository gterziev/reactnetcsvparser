using Microsoft.EntityFrameworkCore;
using ReactNetCsvParser.Server.Data;
using ReactNetCsvParser.Server.Models;
using ReactNetCsvParser.Server.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<WebDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("WebDbContext") ?? throw new InvalidOperationException("Connection string 'WebDbContext' not found.")));

// Add services to the container.
builder.Services.AddSingleton<IParseService<ChartItem>>(new CsvParseService());

const string policyName = "CorsLocalhostPolicy";
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: policyName, builder =>
    {
        builder.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});

builder.Services.AddControllers();

var app = builder.Build();

app.UseDefaultFiles();
app.UseStaticFiles();

app.UseCors(policyName);
app.UseAuthorization();

app.MapControllers();

app.MapFallbackToFile("/index.html");

app.Run();
