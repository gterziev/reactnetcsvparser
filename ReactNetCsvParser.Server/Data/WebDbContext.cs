﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReactNetCsvParser.Server.Models;

namespace ReactNetCsvParser.Server.Data
{
    public class WebDbContext : DbContext
    {
        public WebDbContext (DbContextOptions<WebDbContext> options)
            : base(options)
        {
        }

        public DbSet<AttachmentData> AttachmentData { get; set; } = default!;

        public DbSet<ChartItem> ChartItem { get; set; } = default!;
    }
}
