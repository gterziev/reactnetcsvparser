﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReactNetCsvParser.Server.Models
{
    public class AttachmentData
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? Name { get; set; }

        public DateTime UploadTime {get; set;}

        public List<ChartItem>? ChartDataList { get; set; }
    }
}
