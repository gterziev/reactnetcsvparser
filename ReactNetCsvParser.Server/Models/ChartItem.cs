﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Drawing;

namespace ReactNetCsvParser.Server.Models
{
	public class ChartItem
	{

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string? Color { get; set; }
		public int Number { get; set; }
		public string? Label { get; set; }

	}
}
